// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';

// import 'DataCorona.dart';

// class GetDataApi extends StatefulWidget {
//   @override
//   _GetDataApiState createState() => _GetDataApiState();
// }

// class _GetDataApiState extends State<GetDataApi> {
//   get id => null;
//   // DataCorona _datas;
//   getDataCorona() async {
//     var response =
//         await http.get(Uri.parse("https://api.kawalcorona.com/indonesia"));
//     var jsonData = jsonDecode(response.body);
//     DataCorona datas;
//     for (var u in jsonData) {
//       DataCorona data = DataCorona(
//           u["name"], u["positif"], u["sembuh"], u["meninggal"]);
//       _datas = data;
//     }
//     // print(datas.length);
//     return _datas;
//     // try {
//     //   var response =
//     //       await http.get(Uri.parse("https://api.kawalcorona.com/indonesia"));
//     //   var jsonData = jsonDecode(response.body);
//     //   for (var u in jsonData) {
//     //     DataCorona dc = DataCorona(
//     //         u["name"], u["positif"], u["sembuh"], u["meninggal"], u["dirawat"]);
//     //     data = dc;
//     //   }
//     //   print("data: ${data.name}");
//     //   return data;
//     // } catch (e) {
//     //   print(e);
//     // }
//   }

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     setState(() {
//       getDataCorona();
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Padding(
//       padding: const EdgeInsets.all(20),
//       child: Container(
//         decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.circular(8),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black26,
//               spreadRadius: 2,
//               blurRadius: 10,
//             ),
//           ],
//         ),
//         child: Padding(
//           padding: const EdgeInsets.all(10),
//           child: Column(
//             children: <Widget>[
//               Align(
//                 alignment: Alignment.topLeft,
//                 child: Text(
//                   "Update Corona (${_datas.name})",
//                   style: TextStyle(
//                     color: Colors.black,
//                     fontSize: 16,
//                     fontWeight: FontWeight.bold,
//                     letterSpacing: 1,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 20, right: 10, left: 10),
//                 child: Row(
//                   children: <Widget>[
//                     Container(
//                       child: Center(
//                         child: Column(
//                           children: <Widget>[
//                             Text(
//                               _datas.positif,
//                               style: TextStyle(
//                                   color: Colors.orange,
//                                   fontSize: 15,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             SizedBox(
//                               height: 5,
//                             ),
//                             Text(
//                               "Positif",
//                               style: TextStyle(fontWeight: FontWeight.w300),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 30,
//                     ),
//                     Container(
//                       child: Center(
//                         child: Column(
//                           children: <Widget>[
//                             Text(
//                               _datas.sembuh,
//                               style: TextStyle(
//                                   color: Colors.green,
//                                   fontSize: 15,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             SizedBox(
//                               height: 5,
//                             ),
//                             Text(
//                               "Sembuh",
//                               style: TextStyle(fontWeight: FontWeight.w300),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       width: 30,
//                     ),
//                     Container(
//                       child: Center(
//                         child: Column(
//                           children: <Widget>[
//                             Text(
//                               _datas.meninggal,
//                               style: TextStyle(
//                                   color: Colors.red,
//                                   fontSize: 15,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             SizedBox(
//                               height: 5,
//                             ),
//                             Text(
//                               "Meninggal",
//                               style: TextStyle(fontWeight: FontWeight.w300),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.all(15),
//                 child: Row(
//                   children: <Widget>[
//                     Text("Dirawat : ${_datas.dirawat}"),
//                     SizedBox(
//                       width: 20,
//                     ),
//                     Text("Sembuh : ${_datas.sembuh}")
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     ));
//   }
// }
