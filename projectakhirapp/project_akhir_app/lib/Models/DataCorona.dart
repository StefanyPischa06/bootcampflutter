import 'dart:convert';

class DataCorona {
  final String name, positif, sembuh, meninggal;
  DataCorona(this.name, this.positif, this.sembuh, this.meninggal);
}

DataCoronaModel userModelFromJson(String str) =>
    DataCoronaModel.fromJson(json.decode(str));
String userModelToJson(DataCoronaModel data) => json.encode(data.toJson());

class DataCoronaModel {
  final String name, positif, sembuh, meninggal;
  DataCoronaModel({this.name, this.positif, this.sembuh, this.meninggal});
  factory DataCoronaModel.fromJson(Map<String, dynamic> json) =>
      DataCoronaModel(
        name: json["name"],
        positif: json["positif"],
        sembuh: json["sembuh"],
        meninggal: json["meninggal"],
      );
  Map<String, dynamic> toJson() => {
        "name": name,
        "positif": positif,
        "sembuh": sembuh,
        "meninggal": meninggal,
      };
}
