import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:project_akhir_app/Screen/HalamanScreen.dart';
import 'package:project_akhir_app/bottomMenu.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _emailController;
  String _passwordController;
  bool _autoValidate = false;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(40),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Container(
              alignment: Alignment.topLeft,
              child: Text(
                "News.",
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              "Welcome,",
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.w100),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Sign to continue",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w100),
            ),
            SizedBox(
              height: 30,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    // padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(255, 51, 51, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10))
                        ]),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey[100]))),
                          child: TextFormField(
                            validator: validateEmail,
                            onSaved: (String val) {
                              _emailController = val;
                            },
                            // controller: _emailController,
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.email,
                                  size: 28,
                                  color: Colors.black12,
                                ),
                                border: InputBorder.none,
                                hintText: "Email",
                                hintStyle:
                                    TextStyle(fontWeight: FontWeight.w200)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    // padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(255, 51, 51, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10))
                        ]),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey[100]))),
                          child: TextFormField(
                            validator: validatePassword,
                            onSaved: (String val) {
                              _passwordController = val;
                            },
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.lock,
                                  size: 28,
                                  color: Colors.black12,
                                ),
                                border: InputBorder.none,
                                hintText: "Password",
                                hintStyle:
                                    TextStyle(fontWeight: FontWeight.w200)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: _validateInputs,
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.black,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                    child: Center(child: Text("OR")),
                  ),
                  Container(
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: _validateCreate,
                      child: Text(
                        "Register",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      )),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _validateInputs() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      await _firebaseAuth
          .signInWithEmailAndPassword(
              email: _emailController, password: _passwordController)
          .then((value) => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => BottomMenu())));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Future<void> _validateCreate() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      await _firebaseAuth
          .createUserWithEmailAndPassword(
              email: _emailController, password: _passwordController)
          .then((value) => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => BottomMenu())));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}

String validatePassword(String value) {
  if (value.length < 6)
    return 'Password must be more than 6 charater';
  else
    return null;
}

String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Enter Valid Email';
  else
    return null;
}
