import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:project_akhir_app/Screen/LoginScreen.dart';
import 'package:project_akhir_app/Screen/gridView.dart';

import 'ProfilScreen.dart';

class HalamanScreen extends StatefulWidget {
  @override
  _HalamanScreenState createState() => _HalamanScreenState();
}

class _HalamanScreenState extends State<HalamanScreen> {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser.email);
    }
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Hello,",
                          style: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          auth.currentUser.email,
                          style: TextStyle(
                              color: Color(0xffa29aac),
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 1),
                        ),
                      ],
                    ),
                  ),
                  // IconButton(
                  //     alignment: Alignment.topCenter,
                  //     icon: Icon(Icons.person),
                  //     onPressed: () async {
                  //       Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => ProfilScreen()));
                  //     }),
                  IconButton(
                      alignment: Alignment.topCenter,
                      icon: Icon(Icons.logout),
                      onPressed: () async {
                        await _signOut().then((value) => Navigator.of(context)
                            .pushReplacement(MaterialPageRoute(
                                builder: (context) => LoginScreen())));
                      })
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(255, 51, 51, .2),
                        blurRadius: 7.0,
                        offset: Offset(0, 1))
                  ]),
              child: Container(
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    border:
                        Border(bottom: BorderSide(color: Colors.grey[100]))),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      size: 24,
                      color: Colors.black26,
                    ),
                    border: InputBorder.none,
                    hintText: "Search",
                    hintStyle: TextStyle(color: Colors.black26),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "What news do you want?",
                style: TextStyle(fontWeight: FontWeight.w300),
              ),
            ),
          ),
          GridDashboard()
        ],
      ),
    );
  }
}
