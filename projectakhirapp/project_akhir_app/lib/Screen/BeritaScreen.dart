import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:project_akhir_app/Models/DataCorona.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<DataCorona> getDataCorona() async {
  var response =
      await http.get(Uri.parse("https://api.kawalcorona.com/indonesia"));
  var jsonData = jsonDecode(response.body);
  DataCorona data;
  for (var u in jsonData) {
    DataCorona dc =
        DataCorona(u["name"], u["positif"], u["sembuh"], u["meninggal"]);
    data = dc;
  }
  print("data: ${data.name}");
  return data;
}

class BeritaScreen extends StatefulWidget {
  @override
  _BeritaScreenState createState() => _BeritaScreenState();
}

class _BeritaScreenState extends State<BeritaScreen> {
  List<String> products = [
    "assets/img/c1.jpg",
    "assets/img/sport.jpg",
    "assets/img/corona.jpg"
  ];
  List<String> note = [
    "Politik Amerika",
    "Bulutangkis Indonesia",
    "Update Corona"
  ];
  Future<DataCorona> datas;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    datas = getDataCorona();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: HexColor("#D3D3D3"),
        appBar: AppBar(
          title: Text(
            'News.',
            style: TextStyle(
              fontSize: 25,
              color: Colors.redAccent,
              fontWeight: FontWeight.bold,
              backgroundColor: Colors.white,
            ),
          ),
          backgroundColor: Colors.white,
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 2,
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: FutureBuilder<DataCorona>(
                  future: datas,
                  builder: (context, isi) {
                    if (isi.hasData) {
                      return Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Update Corona (${isi.data.name})",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, right: 10, left: 10),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    child: Center(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            isi.data.positif,
                                            style: TextStyle(
                                                color: Colors.orange,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "Positif",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w300),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Container(
                                    child: Center(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            isi.data.sembuh,
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "Sembuh",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w300),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Container(
                                    child: Center(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            isi.data.meninggal,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "Meninggal",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w300),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    } else if (isi.hasError) {
                      return Text("${isi.error}");
                    }
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 5, bottom: 10),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Breaking News",
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 20,
                      letterSpacing: 0.2),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 2,
                decoration: BoxDecoration(color: Colors.black38),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 20, right: 20),
            //   child: Container(
            //     decoration: BoxDecoration(
            //       color: Colors.white,
            //       borderRadius: BorderRadius.circular(5),
            //       boxShadow: [
            //         BoxShadow(
            //           color: Colors.black26,
            //           spreadRadius: 2,
            //           blurRadius: 10,
            //         ),
            //       ],
            //     ),
            //     child: Column(
            //       children: <Widget>[
            //         Image.asset(
            //           "assets/img/ntt.jpeg",
            //           height: 100,
            //           fit: BoxFit.cover,
            //         ),
            //         Text(
            //           "Update Korban Bencana",
            //           style: TextStyle(fontWeight: FontWeight.bold),
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
            Row(
              children: <Widget>[
                Expanded(
                  child: SizedBox(
                    height: 200.0,
                    child: new ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: products.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(7),
                          child: Container(
                              height: 150,
                              width: 220,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white70),
                              child: Column(
                                children: <Widget>[
                                  Image.asset(
                                    products[index],
                                    fit: BoxFit.cover,
                                    height: 100,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(top: 2, left: 12),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        note[index],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 2, left: 12, right: 14),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Lorem Ipsum is simply dummy text of the printing",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                        );
                      },
                    ),
                  ),
                ),
              ],
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ],
        ));
  }
}
