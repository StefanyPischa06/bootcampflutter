import 'package:flutter/material.dart';
import 'package:project_akhir_app/Screen/ProfilScreen.dart';

import 'BeritaScreen.dart';

// ignore: must_be_immutable
class GridDashboard extends StatelessWidget {
  Items item1 = new Items(
    title: "Update Corona",
    img: Icon(Icons.coronavirus),
  );

  Items item2 = new Items(
    title: "Politik",
    img: Icon(Icons.policy),
  );
  Items item3 = new Items(
    title: "Olahraga",
    img: Icon(Icons.sports_basketball),
  );
  Items item4 = new Items(
    title: "Kesehatan",
    img: Icon(Icons.local_hospital),
  );
  Items item5 = new Items(
    title: "Edukasi",
    img: Icon(Icons.book),
  );
  Items item6 = new Items(
    title: "Teknologi",
    img: Icon(Icons.computer),
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1, item2, item3, item4, item5, item6];
    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                      icon: data.img,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BeritaScreen()));
                      }),
                  SizedBox(
                    height: 14,
                  ),
                  Text(data.title,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w300)),
                ],
              ),
            );
          }).toList()),
    );
  }
}

class Items {
  String title;
  Icon img;
  Items({this.title, this.img});
}
