import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:project_akhir_app/Models/Get_Data.dart';
import 'package:project_akhir_app/Screen/BeritaScreen.dart';
import 'package:project_akhir_app/Screen/HalamanScreen.dart';
import 'package:project_akhir_app/Screen/ProfilScreen.dart';

class BottomMenu extends StatefulWidget {
  @override
  _BottomMenuState createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu> {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  int _selectedIndex = 0;
  final _layoutPage = [
    HalamanScreen(),
    BeritaScreen(),
    ProfilScreen(),
  ];
  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
          BottomNavigationBarItem(
              icon: Icon(Icons.library_books_outlined), title: Text("News")),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), title: Text("Account")),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}
