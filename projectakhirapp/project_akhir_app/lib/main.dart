import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:project_akhir_app/Screen/BeritaScreen.dart';
import 'package:project_akhir_app/Screen/HalamanScreen.dart';
import 'package:project_akhir_app/Screen/LoginScreen.dart';
import 'package:project_akhir_app/Screen/ProfilScreen.dart';
import 'package:project_akhir_app/bottomMenu.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}
