class Segitiga {
  double setengah = 0.5;
  late double alas;
  late double tinggi;

  double luasSegitiga() {
    return setengah * alas * tinggi;
  }
}

void main() {
  Segitiga s = new Segitiga();
  s.alas = 20.0;
  s.tinggi = 30.0;
  print(s.luasSegitiga());
}
