class Lingkaran {
  double _phi = 3.14;
  late double _r;

  double getR() {
    return _r;
  }

  void setR(double r) => _r = r;

  double getPhi() {
    return _phi;
  }

  void setPhi(double phi) => phi = phi;

  double getluasLingkaran() => _phi * _r * _r;
}
