// Soal 1 - Membuat Kalimat
// void main() {
//   var word = 'dart';
//   var second = 'is';
//   var third = 'awesome';
//   var fourth = 'and';
//   var fifth = 'I';
//   var sixth = 'love';
//   var seventh = 'it!';
//   var merge = word +
//       " " +
//       second +
//       " " +
//       third +
//       " " +
//       fourth +
//       " " +
//       fifth +
//       " " +
//       sixth +
//       " " +
//       seventh;
//   print(merge);
// }

// Soal 2 - Menguraikan Kalimat
// void main() {
//   var sentence = "I am going to be Flutter Developer";
//   var exampleFirstWord = sentence[0];
//   var secondWord = sentence[2] + sentence[3];
//   var thirdWord = sentence.substring(5, 10);
//   var fourthWord = sentence[11] + sentence[12];
//   var fifthWord = sentence[14] + sentence[15];
//   var sixthWord = sentence.substring(16, 24); // lakukan sendiri
//   var seventhWord = sentence.substring(25, 34); // lakukan sendiri

//   print('First Word: ' + exampleFirstWord);
//   print('Second Word: ' + secondWord);
//   print('Third Word: ' + thirdWord);
//   print('Fourth Word: ' + fourthWord);
//   print('Fifth Word: ' + fifthWord);
//   print('Sixth Word: ' + sixthWord);
//   print('Seventh Word: ' + seventhWord);
// }

// soal 3 - inputan dinamis
// import 'dart:io';

// void main() {
//   print("Masukan nama depan : ");
//   var nDepan = stdin.readLineSync();
//   print("Masukan nama belakang: ");
//   var nBelakang = stdin.readLineSync();
//   print("Nama lengkap anda adalah: $nDepan $nBelakang");
// }

// soal 4 - operator
void main() {
  int a = 5;
  int b = 10;

  print("Perkalian: ${a * b}");
  print("Pembagian: ${a / b}");
  print("Penambahan: ${a + b}");
  print("Pengurangan: ${a - b}");
}
