// // // import 'dart:async';

// // // void main(List<String> args) {
// // //   print("STarting");
// // //   var timer = Timer(Duration(seconds: -3), () => print("timer completed"));
// // //   print("finis");
// // // }

// // Future<void> printOM() async {
// //   try {
// //     var order = await fetchuser();
// //     print("awaiting user order");
// //     print(order);
// //   } catch (err) {
// //     print("caught error:$err");
// //   }
// // }

// // Future<String> fetchuser() {
// //   var str = Future.delayed(Duration(seconds: 4), () => throw "cannot locate");
// //   return str;
// // }

// // Future<void> main(List<String> args) async {
// //   await printOM();
// // }

// import 'dart:math';

// class Point {
//   double x, y;
//   Point(this.x, this.y);
//   static double distanceBetween(Point a, Point b) {
//     var dx = a.x - b.x;
//     var dy = a.y - b.y;
//     return sqrt(dx * dx + dy * dy);
//   }
// }

// void main(List<String> args) {
//   var a = Point(2, 2);
//   var b = Point(4, 4);
//   var distance = Point.distanceBetween(a, b);
//   assert(2.8 < distance && distance < 2.9);
//   print(distance);
// }

Future<void> printOrder() async {
  print("await");
  var order = await fetchuser();
  print("your order : $order");
}

Future<String> fetchuser() {
  return Future.delayed(Duration(seconds: 4), () => 'large latte');
}

Future<void> main(List<String> args) async {
  count(4);
  await printOrder();
}

void count(int s) {
  for (var i = 1; i <= s; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}
