// soal 1
// teriak() {
//   return "Halo Sanbers!";
// }
// void main() {
//   print(teriak());
// }

// soal 2
// kalikan(x, y) {
//   return x * y;
// }
// void main() {
//   var num1 = 12;
//   var num2 = 4;
//   var hasilKali = kalikan(num1, num2);
//   print(hasilKali);
// }

// soal 3
// introduce(name, age, address, hobby) {
//   return "Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address} dan saya punya hobby yaitu Gaming!";
// }
// void main() {
//   var name = "Agus";
//   var age = 30;
//   var address = "Jln. Malioboro, Yogyakarta";
//   var hobby = "Gaming";
//   var perkenalan = introduce(name, age, address, hobby);
//   print(perkenalan);
// }

// soal 4
factorial(i) {
  int temp = 1;
  if (i <= 1) {
    return 1;
  } else {
    for (int j = 1; j <= i; j++) {
      temp = temp * j;
    }
    return temp;
  }
}

void main() {
  print(factorial(1));
  print(factorial(6));
}
