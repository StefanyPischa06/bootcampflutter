import 'dart:io';

// Soal 1 - Ternary Operator
// void main() {
//   print("Install Aplikasi?: [y/t]");
//   var pilihan = stdin.readLineSync();
//   pilihan == "y"
//       ? print("anda akan menginstall aplikasi dart")
//       : print("aborted");
// }

// Soal 2 - If-else if dan else
// void main() {
//   print("Masukkan nama : ");
//   var nama = stdin.readLineSync();
//   print("Pilihan peran : Penyihir, Guard, dan Werewolf");
//   print("Masukkan pilihan peran: ");
//   var pilihan = stdin.readLineSync();

//   if (nama!.isNotEmpty && pilihan!.isNotEmpty) {
//     if (pilihan == "Penyihir" || pilihan == "penyihir") {
//       print(
//           "Selamat datang di Dunia Werewolf, ${nama} \nHalo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!");
//     } else if (pilihan == "Guard" || pilihan == "guard") {
//       print(
//           "Selamat datang di Dunia Werewolf, ${nama} \nHalo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.");
//     } else if (pilihan == "Werewolf" || pilihan == "werewolf") {
//       print(
//           "Selamat datang di Dunia Werewolf, ${nama} \nHalo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam");
//     } else {
//       print("peran tidak tersedia atau inputan salah");
//     }
//   } else if (nama.isEmpty && pilihan!.isNotEmpty) {
//     print("Nama harus diisi");
//   } else if (nama.isNotEmpty && pilihan!.isEmpty) {
//     print("Halo ${nama}, Pilih peranmu untuk memulai game!");
//   } else {
//     print("Nama dan peran harus diisi");
//   }
// }

// soal 3 - switch case
// void main(List<String> args) {
//   print("Quote harian");
//   print("Masukkan nama hari : ");
//   var pilih = stdin.readLineSync();
//   switch (pilih) {
//     case "Senin":
//       {
//         print(
//             "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
//         break;
//       }
//     case "Selasa":
//       {
//         print(
//             "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
//         break;
//       }
//     case "Rabu":
//       {
//         print(
//             "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
//         break;
//       }
//     case "Kamis":
//       {
//         print(
//             "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
//         break;
//       }
//     case "Jumat":
//       {
//         print("Hidup tak selamanya tentang pacar.");
//         break;
//       }
//     case "Sabtu":
//       {
//         print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
//         break;
//       }
//     case "Minggu":
//       {
//         print(
//             "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani");
//         break;
//       }
//     default:
//       {
//         print("Salah input hari");
//       }
//   }
// }

// soal 4- Switch case
void main(List<String> args) {
  var hari = 21;
  var bulan = 1;
  var tahun = 1945;

  switch (bulan) {
    case 1:
      {
        print("${hari} Januari ${tahun}");
        break;
      }
    case 2:
      {
        print("${hari} Februari ${tahun}");
        break;
      }
    case 3:
      {
        print("${hari} Maret ${tahun}");
        break;
      }
    case 4:
      {
        print("${hari} April ${tahun}");
        break;
      }
    case 5:
      {
        print("${hari} Mei ${tahun}");
        break;
      }
    case 6:
      {
        print("${hari} Juni ${tahun}");
        break;
      }
    case 7:
      {
        print("${hari} July ${tahun}");
        break;
      }
    case 8:
      {
        print("${hari} Agustus ${tahun}");
        break;
      }
    case 9:
      {
        print("${hari} September ${tahun}");
        break;
      }
    case 10:
      {
        print("${hari} Oktober ${tahun}");
        break;
      }
    case 11:
      {
        print("${hari} November ${tahun}");
        break;
      }
    case 12:
      {
        print("${hari} Desember ${tahun}");
        break;
      }
    default:
      {
        print("tanggal tidak tersedia");
      }
  }
}
