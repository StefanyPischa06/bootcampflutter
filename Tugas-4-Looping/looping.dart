import 'dart:io';

// // Soal 1 Looping While
// void main() {
//   //Looping pertama
//   var i = 1;
//   while (i <= 20) {
//     if (i == 1) {
//       print("LOOPING PERTAMA");
//     } else {
//       if (i % 2 == 0) {
//         print("${i} - I love coding");
//       }
//     }
//     i++;
//   }
//   //Looping kedua
//   var j = 21;
//   while (j >= 1) {
//     if (j == 21) {
//       print("LOOPING Kedua");
//     } else {
//       if (j % 2 == 0) {
//         print("${j} - I will become a mobile developer");
//       }
//     }
//     j--;
//   }
// }

// soal 2 - Looping menggunakan for
// void main() {
//   for (int i = 1; i <= 20; i++) {
//     if (i % 2 != 0 && i % 3 == 0 || i % 2 == 0 && i % 3 == 0) {
//       print("${i} - I Love Coding");
//     } else if (i % 2 == 0) {
//       print("${i} - Berkualitas");
//     } else {
//       print("${i} - Santai");
//     }
//   }
// }

// soal 3 - Membuat Persegi Panjang #
// void main() {
//   for (int i = 1; i <= 4; i++) {
//     for (int j = 1; j <= 8; j++) {
//       stdout.write("#");
//     }
//     stdout.write("\n");
//   }
// }

// soal 4 - Membuat tangga
void main() {
  for (int i = 1; i <= 7; i++) {
    for (int j = 1; j <= i; j++) {
      stdout.write("#");
    }
    stdout.write("\n");
  }
}
