import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/LoginScreen.dart';

// import 'Tugas/Tugas14/Get_data.dart';
// import 'package:sanberappflutter/Tugas/Tugas13/HomeScreen.dart';
// import 'Tugas/Tugas12/Telegram.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
