import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 60,
            ),
            Align(
                alignment: Alignment.center,
                child: Text(
                  "Sanber Flutter",
                  style: TextStyle(
                    fontSize: 30,
                    color: HexColor("#54C5F8"),
                    fontWeight: FontWeight.bold,
                  ),
                )),
            SizedBox(
              height: 15,
            ),
            Image.asset(
              "assets/img/flutter.png",
              width: 150,
              height: 140,
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                hintText: "Username",
                hintStyle: TextStyle(color: Colors.black),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                hintText: "Password",
                hintStyle: TextStyle(color: Colors.black),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Align(
                alignment: Alignment.center,
                child: Text(
                  "Forgot Password",
                  style: TextStyle(
                    fontSize: 18,
                    color: HexColor("#54C5F8"),
                  ),
                )),
            SizedBox(
              height: 15,
            ),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: Text("Login"),
                textColor: Colors.white,
                color: HexColor("#54C5F8"),
                onPressed: () {},
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: <Widget>[
                Text("Does not have account?"),
                FlatButton(
                  textColor: HexColor("#54C5F8"),
                  onPressed: () {},
                  child: Text("Sign in"),
                  shape:
                      CircleBorder(side: BorderSide(color: Colors.transparent)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
