import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class HomeScreen extends StatelessWidget {
  List<String> countries = ["Berlin", "Monas", "Roma", "Tokyo"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 60,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                    icon: ImageIcon(AssetImage("assets/img/notif.png")),
                    onPressed: () {}),
                IconButton(
                    icon: ImageIcon(
                        AssetImage("assets/img/add_shopping_cart.png")),
                    onPressed: () {})
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Welcome,",
                  style: TextStyle(
                    fontSize: 40,
                    color: HexColor("#54C5F8"),
                    fontWeight: FontWeight.bold,
                  ),
                )),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Stefany",
                  style: TextStyle(
                    fontSize: 30,
                    color: HexColor("#01579B"),
                  ),
                )),
            SizedBox(
              height: 50,
            ),
            TextField(
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 5.0),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Search",
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Recommended Place",
                )),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 250,
              width: 250,
              child: GridView.count(
                crossAxisCount: 2,
                children: <Widget>[
                  for (var country in countries)
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Image.asset("assets/img/$country.png"),
                    )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
