import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Account.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Home.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Search.dart';

import 'BottomNav.dart';
import 'Login.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // jika ingin mengirim argument
    // final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Login());
      case '/bottom':
        return MaterialPageRoute(builder: (_) => BottomNav());
      case '/home':
        return MaterialPageRoute(builder: (_) => Home());
      case '/search':
        return MaterialPageRoute(builder: (_) => Search());
      case '/account ':
        return MaterialPageRoute(builder: (_) => Account());
      default:
        return _search();
    }
  }

  static Route<dynamic> _search() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Search")),
        body: Center(child: Text('Search')),
      );
    });
  }
}
