import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(icon: Icon(Icons.extension), onPressed: () {})
              ],
            ),
            SizedBox(height: 20),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Welcome,",
                  style: TextStyle(
                    fontSize: 40,
                    color: HexColor("#54C5F8"),
                    fontWeight: FontWeight.bold,
                  ),
                )),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Stefany",
                  style: TextStyle(
                    fontSize: 30,
                    color: HexColor("#01579B"),
                  ),
                )),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, size: 18),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: "Search"),
            ),
            SizedBox(height: 10),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Recommended Place",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                )),
            SizedBox(
              height: 250,
              child: GridView.count(
                crossAxisCount: 2,
                children: <Widget>[
                  for (var country in countries)
                    Padding(
                      padding: const EdgeInsets.only(right: 2.0, bottom: 2.0),
                      child: Image.asset("assets/img/$country.png"),
                    )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final countries = ["Tokyo", "Berlin", "Roma", "Monas", "London", "Paris"];
