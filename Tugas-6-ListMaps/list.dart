// soal 1 - range
// range(starNum, finishNum) {
//   List<int> list = [];
//   for (int i = starNum; i <= finishNum; i++) {
//     list.add(i);
//   }
//   list.sort((a, b) => b - a);
//   return list;
// }

// void main() {
//   print(range(1, 10));
// }

//  soal 2 - range with step
// rangeWithStep(starNum, finishNum, int step) {
//   List<int> list = [];
//   int t = starNum;
//   for (int i = starNum; i <= finishNum; i++) {
//     if (t <= finishNum) {
//       if (i == starNum) {
//         list.add(t);
//       } else {
//         list.add(t);
//       }
//     }
//     t = t + step;
//   }
//   return list;
// }

// void main() {
//   print(rangeWithStep(1, 10, 2));
//   print(rangeWithStep(1, 10, 3));
// }

// soal 3 - List Multidimensi
// dataHandling(input) {
//   for (int i = 0; i < input.length; i++) {
//     for (int j = 0; j < input[i].length; j++) {
//       if (j == 0) {
//         print("Nomor Id: ${input[i][j]}");
//       } else if (j == 1) {
//         print("Nama Lengkap: ${input[i][j]}");
//       } else if (j == 2) {
//         print("TTL: ${input[i][j]} ${input[i][j + 1]}");
//       } else if (j == 4) {
//         print("TTL: ${input[i][j]}");
//       }
//     }
//     print("\n");
//   }
// }
// void main() {
//   var input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
//   ];
//   dataHandling(input);
// }

// soal 4 - Balik kata
balikKata(String kalimat) {
  List<String> list = kalimat.split('');
  return list.reversed.join();
}

void main(List<String> args) {
  print(balikKata("Kasur"));
  print(balikKata("SanberCode"));
  print(balikKata("Haji"));
  print(balikKata("racecar"));
  print(balikKata("Sanbers"));
}
