import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  late double tinggi;
  late double alas;
  late double miring;

  Segitiga(double tinggi, double alas, double miring) {
    this.tinggi = tinggi;
    this.alas = alas;
    this.miring = miring;
  }

  @override
  double luas() {
    // TODO: implement luas
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling() {
    // TODO: implement keliling
    return alas + tinggi + miring;
  }
}
