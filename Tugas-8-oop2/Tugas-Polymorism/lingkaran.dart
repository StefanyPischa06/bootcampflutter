import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  double phi = 3.14;
  late double r;

  Lingkaran(double r) {
    this.r = r;
  }
  @override
  double luas() {
    return phi * r * r;
  }

  @override
  double keliling() {
    return phi * 2 * r;
  }
}
