import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  BangunDatar b = new BangunDatar();
  Lingkaran l = new Lingkaran(7);
  Persegi p = new Persegi(4);
  Segitiga s = new Segitiga(7, 8, 9);

  b.luas();
  b.keliling();

  print("Luas Lingkaran : ${l.luas()} dan Keliling Lingkaran: ${l.keliling()}");
  print("Luas Persegi : ${p.luas()} dan Keliling Persegi: ${p.keliling()}");
  print("Luas Segitiga : ${s.luas()} dan Keliling Segitiga: ${s.keliling()}");
}
