import 'employee.dart';

void main(List<String> args) {
  Employee e = new Employee("1234555", "Stefany Pischa", "IT");
  Employee e1 = new Employee.name("Aku2");
  Employee e2 = new Employee.id("666789");
  Employee e3 = new Employee.departement("Akutansi");

  print("${e.name} - ${e.id} - ${e.departemen}");
  print(e1.name);
  print(e2.id);
  print(e3.departemen);
}
