class Titan {
  late int _powerPoint;

  set powerPoint(int pp) {
    if (pp <= 5) {
      _powerPoint = 5;
    } else {
      _powerPoint = pp;
    }
  }

  int get powerPoint {
    return _powerPoint;
  }
}
