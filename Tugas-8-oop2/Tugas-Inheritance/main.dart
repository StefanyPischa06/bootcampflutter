import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';
import 'titan.dart';

void main() {
  Human h = new Human();
  BeastTitan b = new BeastTitan();
  AttackTitan a = new AttackTitan();
  ArmorTitan at = new ArmorTitan();

  h.powerPoint = 5;
  b.powerPoint = 4;
  a.powerPoint = 1;
  at.powerPoint = 3;

  if (h.powerPoint <= 5) {
    print("${h.powerPoint} => ${h.killAlltitan()}");
  }
  if (b.powerPoint <= 5) {
    print("${b.powerPoint} => ${b.lempar()}");
  }
  if (a.powerPoint <= 5) {
    print("${a.powerPoint} => ${a.punch()}");
  }
  if (at.powerPoint <= 5) {
    print("${at.powerPoint} => ${at.terjang()}");
  }
}
